const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const User = require('../../models/user')

const authenticate = (username) => {
  return jwt.sign(username, process.env.SECRET_TOKEN)
}

exports.login = async (req, res, next) => {
  const { username, password } = req.body
  console.log(username, password)

  const user = await User.findOne({ username })
  console.log(user)
  if(!user) {
    res.status(401).json({ error: "User does not exist"})
  }
  
  const validPassword = await bcrypt.compare(password, user.password)
  if(!validPassword) {
    res.status(401).json({ error: "Invalid Password"})
  }

  // if (username !== 'luan' || password !== '123456') {
  //   return res.json({ success: false ,err: 'Wrong password' })
  // }

  // Fake authen
  const token = jwt.sign(user.username, process.env.SECRET_TOKEN)
  res.json({ success: true, user: { username: 'luan'}, token })
}

exports.register = async (req, res, next) => {
  const { username, email, password, role } = req.body
  const user = new User({
    username,
    email,
    password,
    role
  })

  await user.save()

  res.send({...user, token: authenticate(user.username)})
}