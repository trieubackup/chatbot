exports.faqs = [
  {
    "key": "ask_tuition",
    "link": [
      "https://tuyensinh.ntu.edu.vn/%C4%90e-an-tuyen-sinh/Hoc-phi"
    ],
    "rawContent": [
      "{{school}} là trường đại học công lập, SV đã được hỗ trợ một phần học phí. ĐHNT là một trong những trường có học phí thấp nhất cả nước.",
      "Chương trình đại trà:\nHọc phí khoảng 4 – 5 triệu/1 học kỳ, tùy theo số tín chỉ SV đăng ký học.",
      "Chương trình chất lượng cao:\nCác chương trình song ngữ Anh – Việt; định hướng nghề nghiệp (POHE) – gọi tắt là các chương trình chất lượng cao. Học phí gấp đôi chương trình đại trà, khoảng 10 triệu/học kỳ Học phí được thực hiện theo Nghị định số 86/2015/NĐ - CP ngày 02/10/2015 của Chính phủ “Quy định về cơ chế thu, quản lý học phí đối với cơ sở giáo dục thuộc hệ thống giáo dục quốc dân và chính sách miễn, giảm học phí, hỗ trợ chi phí học tập từ năm học 2015-2016 đến năm học 2020-2021”.\nLộ trình tăng học phí (nếu có) thực hiện theo quy định hiện hành của Nhà nước."
    ],
    "description": "Tiền học phí",
    "__v": 10,
    "content": [
      "ĐH Nha Trang là trường đại học công lập, SV đã được hỗ trợ một phần học phí. ĐHNT là một trong những trường có học phí thấp nhất cả nước.",
      "Chương trình đại trà:\nHọc phí khoảng 4 – 5 triệu/1 học kỳ, tùy theo số tín chỉ SV đăng ký học.",
      "Chương trình chất lượng cao:\nCác chương trình song ngữ Anh – Việt; định hướng nghề nghiệp (POHE) – gọi tắt là các chương trình chất lượng cao. Học phí gấp đôi chương trình đại trà, khoảng 10 triệu/học kỳ Học phí được thực hiện theo Nghị định số 86/2015/NĐ - CP ngày 02/10/2015 của Chính phủ “Quy định về cơ chế thu, quản lý học phí đối với cơ sở giáo dục thuộc hệ thống giáo dục quốc dân và chính sách miễn, giảm học phí, hỗ trợ chi phí học tập từ năm học 2015-2016 đến năm học 2020-2021”.\nLộ trình tăng học phí (nếu có) thực hiện theo quy định hiện hành của Nhà nước."
    ],
    "htmlContent": [
      "<span class=\"mess-constant\"><span class=\"mess-constant__key\">{{school}}</span><span class=\"mess-constant__value\">ĐH Nha Trang</span></span> là trường đại học công lập, SV đã được hỗ trợ một phần học phí. ĐHNT là một trong những trường có học phí thấp nhất cả nước.",
      "Chương trình đại trà:\nHọc phí khoảng 4 – 5 triệu/1 học kỳ, tùy theo số tín chỉ SV đăng ký học.",
      "Chương trình chất lượng cao:\nCác chương trình song ngữ Anh – Việt; định hướng nghề nghiệp (POHE) – gọi tắt là các chương trình chất lượng cao. Học phí gấp đôi chương trình đại trà, khoảng 10 triệu/học kỳ Học phí được thực hiện theo Nghị định số 86/2015/NĐ - CP ngày 02/10/2015 của Chính phủ “Quy định về cơ chế thu, quản lý học phí đối với cơ sở giáo dục thuộc hệ thống giáo dục quốc dân và chính sách miễn, giảm học phí, hỗ trợ chi phí học tập từ năm học 2015-2016 đến năm học 2020-2021”.\nLộ trình tăng học phí (nếu có) thực hiện theo quy định hiện hành của Nhà nước."
    ]
  },
  {
    "key": "ask_tuition_support",
    "link": [
      "https://tuyensinh.ntu.edu.vn/%C4%90e-an-tuyen-sinh/Hoc-phi"
    ],
    "rawContent": [
      "ĐHNT có chính sách ở miễn phí ký túc xá cho thí sinh trúng tuyển và nhập học các ngành sau:\n{{dorm_free_for_major}}",
      "Có chính sách học bổng cho thủ khoa đầu vào cấp trường và ở tất cả các ngành.",
      "Hỗ trợ học bổng cho SV có hoàn cảnh khó khăn."
    ],
    "description": "Hỗ trợ sinh viên khó khăn ",
    "__v": 4,
    "content": [
      "ĐHNT có chính sách ở miễn phí ký túc xá cho thí sinh trúng tuyển và nhập học các ngành sau:\n- Ngành Công nghệ chế biến thủy sản\n- Ngành Quản lý thủy sản\n- Ngành Nuôi trồng thủy sản\n- Ngành Khai thác thủy sản\n- Ngành Khoa học hàng hải",
      "Có chính sách học bổng cho thủ khoa đầu vào cấp trường và ở tất cả các ngành.",
      "Hỗ trợ học bổng cho SV có hoàn cảnh khó khăn."
    ],
    "htmlContent": [
      "ĐHNT có chính sách ở miễn phí ký túc xá cho thí sinh trúng tuyển và nhập học các ngành sau:\n<span class=\"mess-constant\"><span class=\"mess-constant__key\">{{dorm_free_for_major}}</span><span class=\"mess-constant__value\">- Ngành Công nghệ chế biến thủy sản\n- Ngành Quản lý thủy sản\n- Ngành Nuôi trồng thủy sản\n- Ngành Khai thác thủy sản\n- Ngành Khoa học hàng hải</span></span>",
      "Có chính sách học bổng cho thủ khoa đầu vào cấp trường và ở tất cả các ngành.",
      "Hỗ trợ học bổng cho SV có hoàn cảnh khó khăn."
    ]
  },
  {
    "key": "ask_tuition_discount",
    "description": "Chính sách miễn giảm học phí",
    "link": [
      "https://phongctsv.ntu.edu.vn/Che-%C4%91o-Chinh-sach/Che-%C4%91o",
      "https://tuyensinh.ntu.edu.vn/%C4%90e-an-tuyen-sinh/Chinh-sach-uu-tien"
    ],
    "rawContent": [
      "Miễn phí 100% ký túc xá cho thí sinh vào học 5 ngành sau:\nCông nghệ chế biến thủy sản\n{{dorm_free_for_major}}\nĐầu mỗi học kỳ, Nhà trường xem xét miễn giảm khi SV đáp ứng đủ các tiêu chí theo quy định."
    ],
    "__v": 5,
    "content": [
      "Miễn phí 100% ký túc xá cho thí sinh vào học 5 ngành sau:\nCông nghệ chế biến thủy sản\n- Ngành Công nghệ chế biến thủy sản\n- Ngành Quản lý thủy sản\n- Ngành Nuôi trồng thủy sản\n- Ngành Khai thác thủy sản\n- Ngành Khoa học hàng hải\nĐầu mỗi học kỳ, Nhà trường xem xét miễn giảm khi SV đáp ứng đủ các tiêu chí theo quy định."
    ],
    "htmlContent": [
      "Miễn phí 100% ký túc xá cho thí sinh vào học 5 ngành sau:\nCông nghệ chế biến thủy sản\n<span class=\"mess-constant\"><span class=\"mess-constant__key\">{{dorm_free_for_major}}</span><span class=\"mess-constant__value\">- Ngành Công nghệ chế biến thủy sản\n- Ngành Quản lý thủy sản\n- Ngành Nuôi trồng thủy sản\n- Ngành Khai thác thủy sản\n- Ngành Khoa học hàng hải</span></span>\nĐầu mỗi học kỳ, Nhà trường xem xét miễn giảm khi SV đáp ứng đủ các tiêu chí theo quy định."
    ]
  },
  {
    "key": "ask_admission_profile",
    "link": [
      ""
    ],
    "rawContent": [
      "{{school}} năm {{year}} có xét tuyển học bạ",
      "Thí sinh có thể chọn các phương thức xét tuyển sau:\nXét tuyển dựa vào điểm thi THPT năm 2020.\nXét tuyển dựa vào điểm xét tốt nghiệp THPT 2020.\nXét tuyển dựa vào điểm thi Đánh giá năng lực của ĐHQG TP. HCM năm 2020.\nXét tuyển thẳng và ưu tiên xét tuyển theo hình thức riêng của Trường và theo quy chế của Bộ GD&ĐT."
    ],
    "description": "Xét tuyển theo học bạ",
    "__v": 17,
    "content": [
      "ĐH Nha Trang năm 2022 có xét tuyển học bạ",
      "Thí sinh có thể chọn các phương thức xét tuyển sau:\nXét tuyển dựa vào điểm thi THPT năm 2020.\nXét tuyển dựa vào điểm xét tốt nghiệp THPT 2020.\nXét tuyển dựa vào điểm thi Đánh giá năng lực của ĐHQG TP. HCM năm 2020.\nXét tuyển thẳng và ưu tiên xét tuyển theo hình thức riêng của Trường và theo quy chế của Bộ GD&ĐT."
    ],
    "htmlContent": [
      "<span class=\"mess-constant\"><span class=\"mess-constant__key\">{{school}}</span><span class=\"mess-constant__value\">ĐH Nha Trang</span></span> năm <span class=\"mess-constant\"><span class=\"mess-constant__key\">{{year}}</span><span class=\"mess-constant__value\">2022</span></span> có xét tuyển học bạ",
      "Thí sinh có thể chọn các phương thức xét tuyển sau:\nXét tuyển dựa vào điểm thi THPT năm 2020.\nXét tuyển dựa vào điểm xét tốt nghiệp THPT 2020.\nXét tuyển dựa vào điểm thi Đánh giá năng lực của ĐHQG TP. HCM năm 2020.\nXét tuyển thẳng và ưu tiên xét tuyển theo hình thức riêng của Trường và theo quy chế của Bộ GD&ĐT."
    ]
  },
  {
    "key": "ask_admission_multi",
    "link": [
      ""
    ],
    "rawContent": [
      "Thí sinh có thể sử dụng đồng thời các phương thức xét tuyển cho một ngành hoặc nhiều ngành. Tuy nhiên, nên lựa chọn phương thức nào có điểm cao nhất để đăng ký xét tuyển vào ngành mong muốn.",
      "Các phương thức xét tuyển là như nhau trong quá trình thực hiện xét tuyển."
    ],
    "description": "Xét tuyển đa phương thức",
    "__v": 1,
    "content": [
      "Thí sinh có thể sử dụng đồng thời các phương thức xét tuyển cho một ngành hoặc nhiều ngành. Tuy nhiên, nên lựa chọn phương thức nào có điểm cao nhất để đăng ký xét tuyển vào ngành mong muốn.",
      "Các phương thức xét tuyển là như nhau trong quá trình thực hiện xét tuyển."
    ],
    "htmlContent": [
      "Thí sinh có thể sử dụng đồng thời các phương thức xét tuyển cho một ngành hoặc nhiều ngành. Tuy nhiên, nên lựa chọn phương thức nào có điểm cao nhất để đăng ký xét tuyển vào ngành mong muốn.",
      "Các phương thức xét tuyển là như nhau trong quá trình thực hiện xét tuyển."
    ]
  },
  {
    "key": "ask_all_admission",
    "link": [
      ""
    ],
    "rawContent": [
      "Thí sinh có thể sử dụng 1 hoặc đồng thời các phương thức xét tuyển như sau:\n",
      "Xét tuyển dựa vào điểm thi THPT năm 2021\nThang điểm 30\nTối đa 30% tổng chỉ tiêu để xét tuyển tất cả các ngành\n"
    ],
    "description": "Các phương thức xét tuyển",
    "__v": 1,
    "content": [
      "Thí sinh có thể sử dụng 1 hoặc đồng thời các phương thức xét tuyển như sau:\n",
      "Xét tuyển dựa vào điểm thi THPT năm 2021\nThang điểm 30\nTối đa 30% tổng chỉ tiêu để xét tuyển tất cả các ngành\n"
    ],
    "htmlContent": [
      "Thí sinh có thể sử dụng 1 hoặc đồng thời các phương thức xét tuyển như sau:\n",
      "Xét tuyển dựa vào điểm thi THPT năm 2021\nThang điểm 30\nTối đa 30% tổng chỉ tiêu để xét tuyển tất cả các ngành\n"
    ]
  },
  {
    "key": "ask_year_for_graduate",
    "link": [
      ""
    ],
    "rawContent": [
      "Theo kế hoạch học trong {{learning_year}} học, tuy nhiên, Nhà trường tổ chức đào tạo theo hình thức tín chỉ nên SV có thể rút ngắn thời gian học bằng cách đăng ký học vượt; hoặc tùy thuộc vào điều kiện hoàn cảnh bản thân mà có thể kéo dài thời gian học hơn {{learning_year}} năm nhưng không quá 8 năm."
    ],
    "description": "Đào tạo trong mấy năm",
    "__v": 2,
    "content": [
      "Theo kế hoạch học trong 4 năm học, tuy nhiên, Nhà trường tổ chức đào tạo theo hình thức tín chỉ nên SV có thể rút ngắn thời gian học bằng cách đăng ký học vượt; hoặc tùy thuộc vào điều kiện hoàn cảnh bản thân mà có thể kéo dài thời gian học hơn 4 năm năm nhưng không quá 8 năm."
    ],
    "htmlContent": [
      "Theo kế hoạch học trong <span class=\"mess-constant\"><span class=\"mess-constant__key\">{{learning_year}}</span><span class=\"mess-constant__value\">4 năm</span></span> học, tuy nhiên, Nhà trường tổ chức đào tạo theo hình thức tín chỉ nên SV có thể rút ngắn thời gian học bằng cách đăng ký học vượt; hoặc tùy thuộc vào điều kiện hoàn cảnh bản thân mà có thể kéo dài thời gian học hơn <span class=\"mess-constant\"><span class=\"mess-constant__key\">{{learning_year}}</span><span class=\"mess-constant__value\">4 năm</span></span> năm nhưng không quá 8 năm."
    ]
  },
  {
    "key": "ask_another_language",
    "link": [
      ""
    ],
    "rawContent": [
      "Về đào tạo, trường chỉ đào tạo ngành Ngôn ngữ Anh. Bên cạnh đó trường có ngành quản trị dịch vụ du lịch và lữ hành (chương trình song ngữ Pháp-Việt).",
      "Tiếng Anh là ngoại ngữ chủ yếu trong các chương trình đào tạo, tuy nhiên sinh viên có thể lựa chọn học tiếng Trung, Tiếng Pháp, Tiếng Nhật, Tiếng Nga, Tiếng Hàn khi có nhu cầu."
    ],
    "description": "Những ngôn ngữ ngoài tiếng Anh",
    "__v": 1,
    "content": [
      "Về đào tạo, trường chỉ đào tạo ngành Ngôn ngữ Anh. Bên cạnh đó trường có ngành quản trị dịch vụ du lịch và lữ hành (chương trình song ngữ Pháp-Việt).",
      "Tiếng Anh là ngoại ngữ chủ yếu trong các chương trình đào tạo, tuy nhiên sinh viên có thể lựa chọn học tiếng Trung, Tiếng Pháp, Tiếng Nhật, Tiếng Nga, Tiếng Hàn khi có nhu cầu."
    ],
    "htmlContent": [
      "Về đào tạo, trường chỉ đào tạo ngành Ngôn ngữ Anh. Bên cạnh đó trường có ngành quản trị dịch vụ du lịch và lữ hành (chương trình song ngữ Pháp-Việt).",
      "Tiếng Anh là ngoại ngữ chủ yếu trong các chương trình đào tạo, tuy nhiên sinh viên có thể lựa chọn học tiếng Trung, Tiếng Pháp, Tiếng Nhật, Tiếng Nga, Tiếng Hàn khi có nhu cầu."
    ]
  },
  {
    "key": "ask_study_sprint",
    "link": [
      ""
    ],
    "rawContent": [
      "Theo kế hoạch học trong {{learning_year}} năm, tuy nhiên, Nhà trường tổ chức đào tạo theo hình thức tín chỉ nên SV có thể rút ngắn thời gian học bằng cách đăng ký học vượt; hoặc tùy thuộc vào điều kiện hoàn cảnh bản thân mà có thể kéo dài thời gian học hơn {{learning_year}} năm nhưng không quá 8 năm."
    ],
    "description": "Học vượt",
    "__v": 2,
    "content": [
      "Theo kế hoạch học trong 4 năm năm, tuy nhiên, Nhà trường tổ chức đào tạo theo hình thức tín chỉ nên SV có thể rút ngắn thời gian học bằng cách đăng ký học vượt; hoặc tùy thuộc vào điều kiện hoàn cảnh bản thân mà có thể kéo dài thời gian học hơn 4 năm năm nhưng không quá 8 năm."
    ],
    "htmlContent": [
      "Theo kế hoạch học trong <span class=\"mess-constant\"><span class=\"mess-constant__key\">{{learning_year}}</span><span class=\"mess-constant__value\">4 năm</span></span> năm, tuy nhiên, Nhà trường tổ chức đào tạo theo hình thức tín chỉ nên SV có thể rút ngắn thời gian học bằng cách đăng ký học vượt; hoặc tùy thuộc vào điều kiện hoàn cảnh bản thân mà có thể kéo dài thời gian học hơn <span class=\"mess-constant\"><span class=\"mess-constant__key\">{{learning_year}}</span><span class=\"mess-constant__value\">4 năm</span></span> năm nhưng không quá 8 năm."
    ]
  },
  {
    "key": "ask_submit_online",
    "link": [
      ""
    ],
    "rawContent": [
      "Sau khi đăng ký xét tuyển online thí sinh cần nộp hồ sơ về ĐHNT bằng 2 hình thức.",
      "Hình thức 1. Nộp hồ sơ trực tiếp tại Trường Đại học Nha Trang\nThí sinh nộp hồ sơ trực tiếp tại: Phòng Đào tạo Đại học – Trường Đại học Nha Trang\nĐịa chỉ: Số 2 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa.\nĐiện thoại: (0258) 3831145 - (0258) 3831148",
      "Hình thức 2. Nộp hồ sơ qua bưu điện\nThí sinh chuẩn bị hồ sơ đăng ký xét tuyển, đến điểm giao dịch bưu điện chọn một trong hai dịch vụ: chuyển phát nhanh (EMS) hoặc chuyển bưu phẩm bảo đảm.\nThí sinh điền thông tin trên phiếu gửi do bưu điện cung cấp, bao gồm:\n- Họ tên, số báo danh, địa chỉ liên lạc, số điện thoại của thí sinh\n- Số CMND, ngày tháng năm sinh của thí sinh\n- Tên trường: Trường Đại học Nha Trang; Mã trường: TSN\nGửi về địa chỉ: Phòng Đào tạo Đại học - Trường Đại học Nha Trang Số 2 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa\nĐiện thoại: (0258) 3831148"
    ],
    "description": "Đăng ký trực tuyến nộp hồ sơ như thế nào",
    "__v": 1,
    "content": [
      "Sau khi đăng ký xét tuyển online thí sinh cần nộp hồ sơ về ĐHNT bằng 2 hình thức.",
      "Hình thức 1. Nộp hồ sơ trực tiếp tại Trường Đại học Nha Trang\nThí sinh nộp hồ sơ trực tiếp tại: Phòng Đào tạo Đại học – Trường Đại học Nha Trang\nĐịa chỉ: Số 2 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa.\nĐiện thoại: (0258) 3831145 - (0258) 3831148",
      "Hình thức 2. Nộp hồ sơ qua bưu điện\nThí sinh chuẩn bị hồ sơ đăng ký xét tuyển, đến điểm giao dịch bưu điện chọn một trong hai dịch vụ: chuyển phát nhanh (EMS) hoặc chuyển bưu phẩm bảo đảm.\nThí sinh điền thông tin trên phiếu gửi do bưu điện cung cấp, bao gồm:\n- Họ tên, số báo danh, địa chỉ liên lạc, số điện thoại của thí sinh\n- Số CMND, ngày tháng năm sinh của thí sinh\n- Tên trường: Trường Đại học Nha Trang; Mã trường: TSN\nGửi về địa chỉ: Phòng Đào tạo Đại học - Trường Đại học Nha Trang Số 2 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa\nĐiện thoại: (0258) 3831148"
    ],
    "htmlContent": [
      "Sau khi đăng ký xét tuyển online thí sinh cần nộp hồ sơ về ĐHNT bằng 2 hình thức.",
      "Hình thức 1. Nộp hồ sơ trực tiếp tại Trường Đại học Nha Trang\nThí sinh nộp hồ sơ trực tiếp tại: Phòng Đào tạo Đại học – Trường Đại học Nha Trang\nĐịa chỉ: Số 2 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa.\nĐiện thoại: (0258) 3831145 - (0258) 3831148",
      "Hình thức 2. Nộp hồ sơ qua bưu điện\nThí sinh chuẩn bị hồ sơ đăng ký xét tuyển, đến điểm giao dịch bưu điện chọn một trong hai dịch vụ: chuyển phát nhanh (EMS) hoặc chuyển bưu phẩm bảo đảm.\nThí sinh điền thông tin trên phiếu gửi do bưu điện cung cấp, bao gồm:\n- Họ tên, số báo danh, địa chỉ liên lạc, số điện thoại của thí sinh\n- Số CMND, ngày tháng năm sinh của thí sinh\n- Tên trường: Trường Đại học Nha Trang; Mã trường: TSN\nGửi về địa chỉ: Phòng Đào tạo Đại học - Trường Đại học Nha Trang Số 2 Nguyễn Đình Chiểu, Nha Trang, Khánh Hòa\nĐiện thoại: (0258) 3831148"
    ]
  },
  {
    "key": "ask_scholarship_for_valedictorian",
    "link": [
      ""
    ],
    "rawContent": [
      "Có chính sách học bổng cho thủ khoa đầu vào cấp trường và ở tất cả các ngành."
    ],
    "description": "Học bổng cho thủ khoa, á khoa",
    "__v": 1,
    "content": [
      "Có chính sách học bổng cho thủ khoa đầu vào cấp trường và ở tất cả các ngành."
    ],
    "htmlContent": [
      "Có chính sách học bổng cho thủ khoa đầu vào cấp trường và ở tất cả các ngành."
    ]
  },
  {
    "key": "ask_free_dormitory",
    "link": [
      ""
    ],
    "rawContent": [
      "ĐHNT có chính sách ở miễn phí ký túc xá cho thí sinh trúng tuyển và nhập học các ngành sau:\n{{dorm_free_for_major}}"
    ],
    "description": "Miễn phí ký túc xá cho ngành nào",
    "__v": 3,
    "content": [
      "ĐHNT có chính sách ở miễn phí ký túc xá cho thí sinh trúng tuyển và nhập học các ngành sau:\n- Ngành Công nghệ chế biến thủy sản\n- Ngành Quản lý thủy sản\n- Ngành Nuôi trồng thủy sản\n- Ngành Khai thác thủy sản\n- Ngành Khoa học hàng hải"
    ],
    "htmlContent": [
      "ĐHNT có chính sách ở miễn phí ký túc xá cho thí sinh trúng tuyển và nhập học các ngành sau:\n<span class=\"mess-constant\"><span class=\"mess-constant__key\">{{dorm_free_for_major}}</span><span class=\"mess-constant__value\">- Ngành Công nghệ chế biến thủy sản\n- Ngành Quản lý thủy sản\n- Ngành Nuôi trồng thủy sản\n- Ngành Khai thác thủy sản\n- Ngành Khoa học hàng hải</span></span>"
    ]
  },
  {
    "key": "ask_admission_time",
    "link": [
      ""
    ],
    "rawContent": [
      "Hình thức 1: Xét tuyển theo điểm thi THPT năm 2021\nThí sinh đăng ký xét tuyển trực tiếp bằng Phiếu tại Điểm tiếp nhận Hồ sơ đăng ký dự thi (Trường THPT) khi đăng ký thi tốt nghiệp THPT hoặc đăng ký xét tuyển trực tuyến. Thí sinh thực hiện theo kế hoạch Bộ GD&ĐT và của trường THPT nơi đang theo học quy định.\nThời gian: tháng 4/2020 – tháng 5/2020",
      "Hình thức 2: Xét tuyển bằng điểm xét tốt nghiệp THPT năm 2021\nThời gian xét tuyển:\nXét tuyển đợt 1: 20/7/2021 – 30/7/2021, (sau khi thí sinh có Giấy chứng nhận tốt nghiệp tạm thời).\nXét tuyển đợt 2 (nếu có): 8/2021 – 9/2021, thời gian chi tiết sẽ thông báo sau.",
      "Hình thức 3: Xét tuyển bằng điểm thi Đánh giá năng lực ĐHQG TP. HCM năm 2021\nThời gian xét tuyển: 01/5/2021 – 30/7/2021",
      "Hình thức 4: Xét tuyển thẳng và ưu tiên xét tuyển riêng của Trường\nThời gian xét tuyển: 01/5/2021 – 15/7/2021"
    ],
    "description": "Thời gian xét tuyển",
    "__v": 1,
    "content": [
      "Hình thức 1: Xét tuyển theo điểm thi THPT năm 2021\nThí sinh đăng ký xét tuyển trực tiếp bằng Phiếu tại Điểm tiếp nhận Hồ sơ đăng ký dự thi (Trường THPT) khi đăng ký thi tốt nghiệp THPT hoặc đăng ký xét tuyển trực tuyến. Thí sinh thực hiện theo kế hoạch Bộ GD&ĐT và của trường THPT nơi đang theo học quy định.\nThời gian: tháng 4/2020 – tháng 5/2020",
      "Hình thức 2: Xét tuyển bằng điểm xét tốt nghiệp THPT năm 2021\nThời gian xét tuyển:\nXét tuyển đợt 1: 20/7/2021 – 30/7/2021, (sau khi thí sinh có Giấy chứng nhận tốt nghiệp tạm thời).\nXét tuyển đợt 2 (nếu có): 8/2021 – 9/2021, thời gian chi tiết sẽ thông báo sau.",
      "Hình thức 3: Xét tuyển bằng điểm thi Đánh giá năng lực ĐHQG TP. HCM năm 2021\nThời gian xét tuyển: 01/5/2021 – 30/7/2021",
      "Hình thức 4: Xét tuyển thẳng và ưu tiên xét tuyển riêng của Trường\nThời gian xét tuyển: 01/5/2021 – 15/7/2021"
    ],
    "htmlContent": [
      "Hình thức 1: Xét tuyển theo điểm thi THPT năm 2021\nThí sinh đăng ký xét tuyển trực tiếp bằng Phiếu tại Điểm tiếp nhận Hồ sơ đăng ký dự thi (Trường THPT) khi đăng ký thi tốt nghiệp THPT hoặc đăng ký xét tuyển trực tuyến. Thí sinh thực hiện theo kế hoạch Bộ GD&ĐT và của trường THPT nơi đang theo học quy định.\nThời gian: tháng 4/2020 – tháng 5/2020",
      "Hình thức 2: Xét tuyển bằng điểm xét tốt nghiệp THPT năm 2021\nThời gian xét tuyển:\nXét tuyển đợt 1: 20/7/2021 – 30/7/2021, (sau khi thí sinh có Giấy chứng nhận tốt nghiệp tạm thời).\nXét tuyển đợt 2 (nếu có): 8/2021 – 9/2021, thời gian chi tiết sẽ thông báo sau.",
      "Hình thức 3: Xét tuyển bằng điểm thi Đánh giá năng lực ĐHQG TP. HCM năm 2021\nThời gian xét tuyển: 01/5/2021 – 30/7/2021",
      "Hình thức 4: Xét tuyển thẳng và ưu tiên xét tuyển riêng của Trường\nThời gian xét tuyển: 01/5/2021 – 15/7/2021"
    ]
  },
  {
    "key": "ask_join_time",
    "link": [
      ""
    ],
    "rawContent": [
      "Chưa cập nhật"
    ],
    "description": "Thời gian nhập học",
    "__v": 1,
    "content": [
      "Chưa cập nhật"
    ],
    "htmlContent": [
      "Chưa cập nhật"
    ]
  },
  {
    "key": "ask_join_require_file",
    "link": [
      ""
    ],
    "rawContent": [
      "Chưa cập nhật"
    ],
    "description": "Hồ sơ nhập học",
    "__v": 1,
    "content": [
      "Chưa cập nhật"
    ],
    "htmlContent": [
      "Chưa cập nhật"
    ]
  },
  {
    "key": "ask_all_major",
    "link": [],
    "rawContent": [
      "Hiện tại Trường Đại họccc Nha Trang đang tạo tạo 36 ngành(50 chuyên ngành/chương trình đào tạo):\n{{all_major}}\n"
    ],
    "description": "Hỏi các ngành đang đào tạo",
    "__v": 2,
    "content": [
      "Hiện tại Trường Đại họccc Nha Trang đang tạo tạo 36 ngành(50 chuyên ngành/chương trình đào tạo):\n- Quản trị kinh doanh (chỉ tiêu: 30 sinh viên})\n- Kế toán (chỉ tiêu: 30 sinh viên})\n- Công nghệ thông tin (chỉ tiêu: 30 sinh viên})\n- Quản trị khách sạn (chỉ tiêu: 60 sinh viên})\n- Khai thác thuỷ sản (chỉ tiêu: 50 sinh viên})\n- Quản lý thuỷ sản (chỉ tiêu: 50 sinh viên})\n- Nuôi trồng thuỷ sản (chỉ tiêu: 160 sinh viên})\n- Công nghệ sinh học (chỉ tiêu: 60 sinh viên})\n- Kỹ thuật môi trường (chỉ tiêu: 50 sinh viên})\n- Kỹ thuật cơ khí (chỉ tiêu: 80 sinh viên})\n- Công nghệ chế tạo máy (chỉ tiêu: 60 sinh viên})\n- Kỹ thuật cơ điện tử (chỉ tiêu: 80 sinh viên})\n- Kỹ thuật nhiệt (chỉ tiêu: 80 sinh viên})\n- Khoa học hàng hải (chỉ tiêu: 50 sinh viên})\n- Kỹ thuật cơ khí động lực (chỉ tiêu: 50 sinh viên})\n- Kỹ thuật tàu thủy (chỉ tiêu: 80 sinh viên})\n- Kỹ thuật ô tô (chỉ tiêu: 180 sinh viên})\n- Kỹ thuật điện (chỉ tiêu: 140 sinh viên})\n- Kỹ thuật xây dựng (chỉ tiêu: 180 sinh viên})\n- Kỹ thuật hoá học (chỉ tiêu: 50 sinh viên})\n- Công nghệ thực phẩm (chỉ tiêu: 180 sinh viên})\n- Công nghệ chế biến thuỷ sản (chỉ tiêu: 60 sinh viên})\n- Công nghệ thông tin (chỉ tiêu: 220 sinh viên})\n- Hệ thống thông tin quản lý (chỉ tiêu: 50 sinh viên})\n- Quản trị dịch vụ du lịch và lữ hành (chỉ tiêu: 30 sinh viên})\n- Quản trị dịch vụ du lịch và lữ hành (chỉ tiêu: 150 sinh viên})\n- Quản trị khách sạn (chỉ tiêu: 200 sinh viên})\n- Quản trị kinh doanh (chỉ tiêu: 180 sinh viên})\n- Marketing (chỉ tiêu: 110 sinh viên})\n- Kinh doanh thương mại (chỉ tiêu: 110 sinh viên})\n- Tài chính - ngân hàng (chỉ tiêu: 110 sinh viên})\n- Kế toán (chỉ tiêu: 160 sinh viên})\n- Luật  (chỉ tiêu: 70 sinh viên})\n- Ngôn ngữ Anh (chỉ tiêu: 200 sinh viên})\n- Kinh tế (chỉ tiêu: 50 sinh viên})\n- Kinh tế phát triển (chỉ tiêu: 70 sinh viên})\n"
    ],
    "htmlContent": [
      "Hiện tại Trường Đại họccc Nha Trang đang tạo tạo 36 ngành(50 chuyên ngành/chương trình đào tạo):\n<span class=\"mess-constant\"><span class=\"mess-constant__key\">{{all_major}}</span><span class=\"mess-constant__value\">- Quản trị kinh doanh (chỉ tiêu: 30 sinh viên})\n- Kế toán (chỉ tiêu: 30 sinh viên})\n- Công nghệ thông tin (chỉ tiêu: 30 sinh viên})\n- Quản trị khách sạn (chỉ tiêu: 60 sinh viên})\n- Khai thác thuỷ sản (chỉ tiêu: 50 sinh viên})\n- Quản lý thuỷ sản (chỉ tiêu: 50 sinh viên})\n- Nuôi trồng thuỷ sản (chỉ tiêu: 160 sinh viên})\n- Công nghệ sinh học (chỉ tiêu: 60 sinh viên})\n- Kỹ thuật môi trường (chỉ tiêu: 50 sinh viên})\n- Kỹ thuật cơ khí (chỉ tiêu: 80 sinh viên})\n- Công nghệ chế tạo máy (chỉ tiêu: 60 sinh viên})\n- Kỹ thuật cơ điện tử (chỉ tiêu: 80 sinh viên})\n- Kỹ thuật nhiệt (chỉ tiêu: 80 sinh viên})\n- Khoa học hàng hải (chỉ tiêu: 50 sinh viên})\n- Kỹ thuật cơ khí động lực (chỉ tiêu: 50 sinh viên})\n- Kỹ thuật tàu thủy (chỉ tiêu: 80 sinh viên})\n- Kỹ thuật ô tô (chỉ tiêu: 180 sinh viên})\n- Kỹ thuật điện (chỉ tiêu: 140 sinh viên})\n- Kỹ thuật xây dựng (chỉ tiêu: 180 sinh viên})\n- Kỹ thuật hoá học (chỉ tiêu: 50 sinh viên})\n- Công nghệ thực phẩm (chỉ tiêu: 180 sinh viên})\n- Công nghệ chế biến thuỷ sản (chỉ tiêu: 60 sinh viên})\n- Công nghệ thông tin (chỉ tiêu: 220 sinh viên})\n- Hệ thống thông tin quản lý (chỉ tiêu: 50 sinh viên})\n- Quản trị dịch vụ du lịch và lữ hành (chỉ tiêu: 30 sinh viên})\n- Quản trị dịch vụ du lịch và lữ hành (chỉ tiêu: 150 sinh viên})\n- Quản trị khách sạn (chỉ tiêu: 200 sinh viên})\n- Quản trị kinh doanh (chỉ tiêu: 180 sinh viên})\n- Marketing (chỉ tiêu: 110 sinh viên})\n- Kinh doanh thương mại (chỉ tiêu: 110 sinh viên})\n- Tài chính - ngân hàng (chỉ tiêu: 110 sinh viên})\n- Kế toán (chỉ tiêu: 160 sinh viên})\n- Luật  (chỉ tiêu: 70 sinh viên})\n- Ngôn ngữ Anh (chỉ tiêu: 200 sinh viên})\n- Kinh tế (chỉ tiêu: 50 sinh viên})\n- Kinh tế phát triển (chỉ tiêu: 70 sinh viên})</span></span>\n"
    ],
    "order": -1
  },
  {
    "key": "ask_admission_english_requirement",
    "link": [
      ""
    ],
    "rawContent": [
      "Chưa cập nhật"
    ],
    "description": "Điểm sàn tiếng Anh cho những ngành",
    "__v": 9,
    "content": [
      "Chưa cập nhật"
    ],
    "htmlContent": [
      "Chưa cập nhật"
    ]
  },
  {
    "key": "ask_admission_english_certificate",
    "link": [
      ""
    ],
    "rawContent": [
      "Chưa cập nhật"
    ],
    "description": "Xét tuyển thẳng theo chứng chỉ tiếng Anh quốc tế",
    "__v": 9,
    "content": [
      "Chưa cập nhật"
    ],
    "htmlContent": [
      "Chưa cập nhật"
    ]
  },
  {
    "key": "ask_submit_at_school",
    "link": [
      ""
    ],
    "rawContent": [
      "Chưa cập nhật"
    ],
    "description": "Đăng ký tại trường",
    "__v": 9,
    "content": [
      "Chưa cập nhật"
    ],
    "htmlContent": [
      "Chưa cập nhật"
    ]
  },
  {
    "key": "ask_submit_require_profile",
    "link": [
      ""
    ],
    "rawContent": [
      "Chưa cập nhật"
    ],
    "description": "Những hồ sơ cần thiết khi đăng ký",
    "__v": 9,
    "content": [
      "Chưa cập nhật"
    ],
    "htmlContent": [
      "Chưa cập nhật"
    ]
  }
]