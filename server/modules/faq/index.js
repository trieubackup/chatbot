const router = require('express').Router()

const Controller = require('./controller')

router.get('/all', Controller.all)
router.get('/:key', Controller.getAnswer)
router.post('/update/:id', Controller.updateAnswer)
router.get('/fb/:key', Controller.getAnswerFB)
router.get('/convert', Controller.convertAllContent)

router.get('/rasa/get-answer', Controller.getAnswerRasa)

module.exports = router