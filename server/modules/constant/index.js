const express = require('express')

const router = express.Router()

const Controller = require('./controller')

router.get('/', Controller.all)
router.get('/:key', Controller.one)
router.post('/create', Controller.create)
router.post('/update/:key', Controller.update)
router.delete('/delete/:key', Controller.delete)

module.exports = router