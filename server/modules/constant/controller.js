const Helper = require('../../helper')

const FAQ = require('../../models/faq')
const Constant = require('../../models/constant')

exports.all = async (req, res) => {
  const constants = await Constant.find({})
  res.send(constants)
}

exports.one = async (req, res) => {
  const {
    key
  } = req.params
  const constant = await Constant.findOne({
    key
  })
  res.send(constant)
}

exports.create = async (req, res) => {
  const {
    key,
    name,
    value
  } = req.body

  const newConstant = new Constant({
    key,
    name,
    value
  })

  newConstant.save(err => {
    if (err) res.send({
      success: false
    })
  })

  const faqs = await FAQ.find()

  faqs.map(async (faq) => {
    await Helper.convertAnswer(faq)
    await faq.save()
  })

  res.send({
    success: true
  })
}

exports.update = async (req, res) => {
  const {
    _id,
    key,
    name,
    value
  } = req.body

  let constant = await Constant.findById(_id)

  if (constant) {
    await Constant.findByIdAndUpdate(_id, { key, name, value })

    const faqs = await FAQ.find()

    faqs.map(async (faq) => {
      await Helper.convertAnswer(faq)
      await faq.save()
    })

    return res.send({
      success: true
    })
  }
  return res.send({
    success: false
  })
}

exports.delete = async (req, res) => {
  return res.send({
    success: true
  })
}