const router = require('express').Router()
const Controller = require('./controller')

router.get('/get-answer/:key', Controller.getAnswer)
router.get('/get-major/:key', Controller.getMajor)

module.exports = router