const Mongoose = require('mongoose')
require('dotenv').config()

const MONGO_HOST = process.env.MONGO_HOST || 'localhost'

Mongoose.connect(`mongodb://${MONGO_HOST}:27017/rasa`, {
  useCreateIndex: true,
  useFindAndModify: true,
  useUnifiedTopology: true,
  useNewUrlParser: true
})
const db = Mongoose.connection
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', function callback() {
  console.log(`Connected to MongoDB at http://${MONGO_HOST}:27017`)
})

exports.Mongoose = Mongoose
exports.db = db