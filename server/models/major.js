const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MajorSchema = new Schema({
  majorId: { type: String, required: true, unique: true },
  majorName: { type: String, required: true },
  program: { type: [String] },
  specialized: { type: [String] },
  combination: { type: [String] },
  isHighQuality: { type: Boolean, default: false },
  target: { type: Number },
  requireEnglish: { type: Boolean, default: false }
}, {
  collection: 'majors'
})

module.exports = mongoose.model('majors', MajorSchema)