const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true }
}, {
  collection: 'users'
})

UserSchema.pre('save', async function(next) {
  const user = this
  if(!user.isModified('password')) {
    return next()
  }
  try {
    const salt = await bcrypt.genSalt(10)
    user.password = await bcrypt.hash(user.password, salt)
    return next()
  } catch(error) {
    return next(error)
  } 
})

UserSchema.methods.comparePassword = async function(password, cb) {
  const user = this
  bcrypt.compare(password, user.password, function(err, isMatch) {
    if(err) return cb(err)
    cb(null, isMatch)
  })
}

module.exports = mongoose.model('users', UserSchema)