const mongoose = require('mongoose')
const Schema = mongoose.Schema

const FaqSchema = new Schema({
  key: { type: String, required: true, unique: true },
  link: { type: [String] },
  rawContent: { type: [String] },
  htmlContent: { type: [String] },
  content: { type: [String] },
  description: { type: String }
}, {
  collection: 'faqs'
})

module.exports = mongoose.model('faqs', FaqSchema)