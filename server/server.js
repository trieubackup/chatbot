const express = require('express')
const bodyParser= require('body-parser')
const cors = require('cors')
const dotenv = require('dotenv')

const app = express()
dotenv.config()
const port = process.env.PORT || 3030

const endpoints = require('./modules/endpoints')

const db = require('./db')

app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 
app.use(cors())

// app.use(endpoint)
app.use('', endpoints)

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`)
})