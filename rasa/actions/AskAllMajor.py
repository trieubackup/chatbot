from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import requests

class ActionAllMajor(Action):
	def name(self) -> Text:
		return "action_all_major"

	def run(self, dispatcher: CollectingDispatcher,
			tracker: Tracker,
			domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
		res = requests.get('http://cms-server:3030/get-answer/ask_all_major').json()
		
		for item in res['content']:
			dispatcher.utter_message(item)

		return []