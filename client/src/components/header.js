import React from 'react'
import { NavLink } from "react-router-dom"
import { removeAccessToken } from '../helpers'

import { connect } from 'react-redux'
import { removeUser } from '../redux/User/user.actions'

const Header = (props) => {
  const { user, removeUser } = props

  const logout = (e) => {
    removeAccessToken()
    removeUser()
  }

  return (
    <nav className="header border-bottom bg-primary flex justify-content-between align-items-center">
      <div className="navbar text-white">
        <li>
          <NavLink exact activeClassName="active" to="/">Trang chủ</NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/major">Ngành đào tạo</NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/faq">Bộ câu hỏi</NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/constant">Hệ thống</NavLink>
        </li>
        <li>
          <NavLink activeClassName="active" to="/conversation">Hội thoại</NavLink>
        </li>
      </div>
      {user.isActive &&
      <div className="flex align-items-center">
        <h4 className="mr-8 text-white">Xin chào, {user.username}</h4>
        <button className="btn bg-white border-white text-primary" onClick={(e) => logout(e)}>Đăng xuất</button>
      </div>
      }
    </nav>
  )   
}

const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    removeUser: () => dispatch(removeUser())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Header)