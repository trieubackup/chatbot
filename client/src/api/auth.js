import axios from 'axios'

import { saveAccessToken } from '../helpers'

const api = axios.create({
  baseURL:'/api'
})

const login = async (payload) => api.post('/auth/login', payload)
  .then(({data}) => data)

const logout = () => {
}
export default {
  login,
  logout,
}