import axios from 'axios'
import auth from './auth'
import { getAccessToken } from '../helpers'

const api = axios.create({
  baseURL:'/api'
})

api.interceptors.request.use(config => {
  config.headers.Authorization = `Bearer ${getAccessToken()}`
  return config
})
api.interceptors.response.use(response => {
  return response
})


const sync = async (token) => api.get('/auth/sync/', { token }).then(({data}) => data)

// Constant
const getConstants = async () => api.get('/constant')
const getConstant = async (key) => api.get(`/constant/${key}`)
const createConstant = async (payload) => api.post('/constant/create', payload)
const updateConstant = async (id, payload) => api.post(`/constant/update/${id}`, payload)

// FAQ
const getAnswers = async () => api.get('/faq/all')
const getAnswer = async (key) => api.get(`/faq/${key}`)
const getFbAnswer = async (payload) => api.post('/faq/fb', payload)
const updateAnswer = async (id, payload) => api.post(`/faq/update/${id}`, payload)

// Major

const API = {
  // Auth
  ...auth,
  sync,

  // Constant
  getConstants,
  getConstant,
  createConstant,
  updateConstant,

  // FAQ
  getAnswers,
  getAnswer,
  getFbAnswer,
  updateAnswer

  // Major
}

export default API