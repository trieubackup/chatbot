import {
  REMOVEUSER,
  SETUSER,
} from './user.types'

const INIT_STATE = {
  isActive: false
}

const reducer = (state = INIT_STATE, action) => {
  switch(action.type) {
    case SETUSER:
      return {
        ...state,
        ...action.payload.user,
        isActive: true
      }

    case REMOVEUSER:
      return {
        isActive: false
      }

    default:
      return state
  }
}

export default reducer