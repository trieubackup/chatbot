import {
  SETUSER,
  REMOVEUSER,
} from './user.types'

export const setUser = (user) => {
  return {
    type: SETUSER,
    payload: {
      user
    }
  }
}

export const removeUser = () => {
  return {
    type: REMOVEUSER
  }
}