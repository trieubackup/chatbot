import {
  SETMESS,
  SETLOADING
} from './messenger.types'

const INIT_STATE = {
  key: '',
  content: [],
  htmlContent: [],
  isLoading: false
}

const reducer = (state = INIT_STATE, action) => {
  switch(action.type) {
    case SETMESS:
      return {
        ...state,
        ...action.payload.mess
      }

    case SETLOADING:
      return {
        ...state,
        isLoading: action.payload.value
      }

    default:
      return state
  }
}

export default reducer