import { createStore } from 'redux'
import rootReducer from './rootProducer'

const store = createStore(rootReducer)

export default store