export const saveAccessToken = (token) => {
  localStorage.setItem('token', token)
}

export const getAccessToken = () => {
  return localStorage.getItem('token')
}

export const removeAccessToken = () => {
  localStorage.removeItem('token')
}