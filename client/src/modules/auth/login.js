import React, { useState } from 'react'

import API from '../../api'
import { saveAccessToken } from '../../helpers'

import { connect } from 'react-redux'
import { setUser } from '../../redux/User/user.actions'

const Login = (props) => {
  const { user, setUser } = props

  const [form, setForm] = useState({
    username: '',
    password: '',
  })

  const formSubmit = async (e) => {
    e.preventDefault()
    const data = await API.login(form)
    if (data.success) {
      setUser(data.user)
      saveAccessToken(data.token)
    }
  }

  const formChange = ({target}) => {
    const { name } = target
    const value = target.type === 'checkbox' ? target.checked : target.value
    setForm(prev => ({
      ...prev,
      [name]: value
    }))
  }

  return (
    <div className="flex-1 bg-secondary">
      <div className="login-container bg-white border">
        <div className="login-header border-bottom">
          <h2 className="text-center">NTU - Nha Trang University</h2>
        </div>
        <div className="flex flex-row login-body px-32 py-32">
          <form className="width-50 px-16" onSubmit={(e) => formSubmit(e)}>
            <div>
              <input className="width-100 p-8" autoFocus name="username" tabIndex="0" onChange={(e) => formChange(e)} placeholder="Tên tài khoản" />
            </div>
            <div>
              <input className="width-100 p-8 mt-16" name="password" type="password" onChange={(e) => formChange(e)} placeholder="Mật khẩu" />
            </div>
            <input className="width-100 mt-32 btn bg-primary text-white uppercase font-bold" type="submit" onClick={(e) => formSubmit(e)} value="Login" />
          </form>
          <div className="width-50 px-16 login-note">
            <a>Bạn quên kí danh hoặc mật khẩu?</a>
            <p className="mt-16">Trình duyệt của bạn cần phải mở chức năng quản lí cookie</p>
            <p className="mt-16">Có thể có một số khoá học cho phép khách vãng lai truy cập</p>
          </div>
        </div>
      </div>
    </div>
  )
}
const mapStateToProps = (state) => {
  return {
    user: state.user
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setUser: (user) => { dispatch(setUser(user)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)