import React, {useState, useEffect } from 'react'
import {
  useParams
} from "react-router-dom";
import API from '../../api'

import { connect } from 'react-redux'
import { setMessenger, setLoading } from '../../redux/Messenger/messenger.actions'

const FaqEditor = (props) => {

  const { messenger, setMessenger, setLoading } = props

  const [state, setState] = useState({
    key: useParams().key,
    description: '',
    link: []
  })
  const [content, setContent] = useState([])

  const { _id, key, description, link } = state

useEffect(() => {
    const fetchData = async () => {
      const {data} = await API.getAnswer(key)
      if (data) {
        const { _id, key, description, link } = data
        setState({
          _id,
          key,
          description,
          link,
          isLoadSuccess: true
        })
        setContent(data.rawContent)
        setMessenger({
          key: data.key,
          content: data.content,
          htmlContent: data.htmlContent,
          isLoading: false
        })
      }
    }
    fetchData()
  }, [])

  const onContentChange = (e, index) => {
    const { target } = e
    content[index] = target.textContent
  }

  const addContent = (e) => {
    setContent(prev => [...prev, ""])
  }

  const removeContent = (e, index) => {
    setContent(prev => [
      ...prev.slice(0, index),
      ...prev.slice(index + 1)
    ])
  }

  const updateContent = async (e) => {
    e.preventDefault()

    const payload = {
      key,
      description,
      link,
      rawContent: content
    }
    
    setLoading()
    const {data} = await API.updateAnswer(_id, payload)
    if(data.success) {
      setMessenger({
        content: data.content,
        htmlContent: data.htmlContent,
        isLoading: false
      })
    }
  }

  return (
    <div className="faq-editor">
      <div className="btn-group flex justify-content-between mb-16">
        <button className="btn back">Trở về</button>
        <button className="btn btn-primary save" onClick={(e) => updateContent(e)}>Lưu</button>
      </div>
      <div className="form">
        <div className="form-input">
          <label>Mã</label>
          <input value={key} disabled />
        </div>
        
        <div className="form-input">
          <label>Mô tả</label>
          <input value={description} onChange={(e) => {onContentChange(e)}} />
        </div>
        
        <div className="form-input">
          <label>Nội dung</label>
          <div className="list-content mt-8">
          {
            content.map((item, index) => 
              <div className="content relative"
              key={index}>
                <div
                  className="textarea bg-white mt-8 pointer editable"
                  contentEditable="true"
                  spellCheck="false"
                  onBlur={(e) => {onContentChange(e, index)}}
                  suppressContentEditableWarning={true}
                >
                  {item}
                </div>
                <button className="del circle bg-primary text-white pointer" onClick={(e) => {removeContent(e, index)}}>x</button>
              </div>
              
            )
          }
          </div>
        </div>
        <button className="btn btn-primary btn-center" onClick={(e) => addContent(e)}>+</button>
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    messenger: state.messenger
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setMessenger: (mess) => dispatch(setMessenger(mess)),
    setLoading: (value = true) => dispatch(setLoading(value))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FaqEditor)