import React, { useState, useEffect } from 'react'
import MessengerContext from '../../MessengerContext'
import Modal from './constant.modal'
import API from '../../api'

import { connect } from 'react-redux'
import { setMessenger } from '../../redux/Messenger/messenger.actions'

const Constant = (props) => {

  const { messenger, setMessenger } = props

  const [constant, setConstant] = useState([])
  const [selected, setSelected] = useState('')
  const [modalState, setModalState] = useState(false)
  const [form, setForm] = useState({
    _id: '',
    key: '',
    name: '',
    value: ''
  })

  const fetchData = async() => {
    const { data } = await API.getConstants()
    setConstant(data)
  }

  useEffect(() => {
    fetchData()
  }, [])

  const onRowClick = (e, item) => {
    switch(e.detail) {
      case 1:
        setSelected(item.key)
        break
      case 2:
        openEditModal(item)
        break
      default:
    }
  }

  const openEditModal = item => {
    setForm({
      ...item
    })
    setModalState(true)
  }

  const handleInputChange = e => {
    const { target } = e
    const { name, value } = target
    setForm(prev => ({
      ...prev,
      [name]: value
    }))
  }

  const handleFormSubmit = async (e) => {
    if(form._id) {
      await API.updateConstant(form._id,form)
    } else {
      await API.createConstant(form)
    }
    await fetchData()

    const {data} = await API.getAnswer(messenger.key)
    setMessenger({
      key: data.key,
      content: data.content,
      htmlContent: data.htmlContent,
      isLoading: false
    })
    
    setModalState(false)
  }

  const Table = () => {
    return (
      <div className="table bg-white flex flex-col flex-1">
        <div className="table-row table-header flex">
          <div className="width-30 font-bold uppercase">Mã</div>
          <div className="width-30 font-bold uppercase">Tên gọi</div>
          <div className="width-40 font-bold uppercase">Giá trị</div>
        </div>
        <div className="table-body">
        { constant.map(item => (
          <div
            className={`table-row flex pointer ${selected === item.key ? 'selected' : ''}`}
            key={item.key}
            onClick={(e) => onRowClick(e, item)}
          >
            <div className="width-30">{item.key}</div>
            <div className="width-30">{item.name}</div>
            <div className="width-40 pre-wrap">
              {item.value}
            </div>
          </div>
        ))}
        </div>
      </div>
    )
  }

  return (
    <div className="constant">
      <div className="title mb-16 flex align-items-center">
        <h1 className="uppercase mr-8">Hệ thống</h1>
        <button className="btn add" onClick={(e) => { setModalState(true)}}>+</button>
      </div>
      { constant.length && <Table /> }
      {modalState &&
      <Modal
        formData={form}
        onChange={(e) => handleInputChange(e)}
        onSubmit={() => {handleFormSubmit()}}
        onClose={() => {setModalState(false)}}
      />
      }
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    messenger: state.messenger
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    setMessenger: (mess) => dispatch(setMessenger(mess))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Constant)