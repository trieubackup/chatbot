import Axios from 'axios'
const apiUrl = 'http://localhost:3000'

const getConstants = () => Axios.get(`${apiUrl}/get-constants`)
const createConstant = (payload) => Axios.post(`${apiUrl}/create-constant`, {
    ...payload
})

export default {
    getConstants,
    createConstant
}